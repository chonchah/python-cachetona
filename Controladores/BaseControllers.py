#encoding=utf-8
import tornado.web
import tornado.autoreload
import traceback, sys
from pymongo import MongoClient
from bson import json_util as JSON

global _env, static_path_dir, m_cache

class MongoCache:
    def __init__(self,db_env):
        self.client   =  MongoClient(
            host        =   db_env["HOST"],
            username    =   db_env["USER"],
            password    =   db_env["PASS"],
            authSource  =   db_env["DB"  ],
        )
        self.db=self.client[db_env["DB"]]
        self.huateque={}
        print ("Abierta conexion", self.client)
    def obtenerDocumento(self, collection, _id):
        c_h,h=hash(collection.encode()),hash(_id.encode())
        print("Busca",c_h, h, collection)
        if not (c_h in self.huateque and h in self.huateque[c_h] ) : return
        return self.huateque[c_h][h]

    def loadCollection(self, collection, max_docs=None):
        coll=self.db[collection]
        docs = coll.find()
        if max_docs: docs=docs.limit(max_docs)
        cache_dict={}
        for doc in docs:
            cache_dict[hash(str(doc['_id']))]= JSON.dumps( doc)
        self.huateque[hash(collection)]=cache_dict
        
        return cache_dict
        

class ErrorBaseHandler(tornado.web.RequestHandler):
    def __init__(self,app, rqst):
        super(ErrorBaseHandler, self).__init__(app,rqst)
    def write_error(self, status_code, **kwargs):
        
        print("Hay un error %d Yo me hago cargo ahora"%status_code)
        
        if kwargs.get("reason"): self._reason=kwargs.get("reason")
        self.set_status(status_code, self._reason)

        if self.settings.get("serve_traceback") and "exc_info" in kwargs:
            # in debug mode, try to send a traceback
            self.set_header("Content-Type", "text/plain")
            for line in traceback.format_exception(*kwargs["exc_info"]):
                self.write(line)
            self.finish()
        else:
            self.finish(
                "<html><title>%(code)d: %(message)s</title>"
                "<body>Error %(code)d del protocolo HTTP: %(message)s</body></html>"
                % {"code": status_code, "message": self._reason}
            )

