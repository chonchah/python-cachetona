import sys
class Ecosistema:
    def __init__(self):
        self._env = eval(open("%s/.env"%sys.path[0]).read())
        self._conf = eval(open("%s/Configuracion/app.py"%sys.path[0]).read())

    def env(self,k, default=None):
        if not k in self._env: return default
        return self._env[k]
    def conf(self,k, default=None):
        if not k in self._conf: return default
        return self._conf[k]