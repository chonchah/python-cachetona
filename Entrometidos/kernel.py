import sys
sys.path.append(sys.path[0]+'../')
from Entrometidos.middlewares import AccessToken
entrometidos={
    'claveToken': AccessToken.AccessToken
}
def entrometerse(handle, entros: list):
    if len(entros):
        print(entros[0])
        return entrometidos[entros.pop(0)]().handle(handle, entros)