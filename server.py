#encoding=utf-8
import tornado.web
import tornado.autoreload
import traceback, sys
from Rutas.api import RUTAS_API

if __name__ == '__main__':
    app = tornado.web.Application(RUTAS_API)
    app.listen(9001)
    tornado.autoreload.start()
    tornado.ioloop.IOLoop.instance().start()