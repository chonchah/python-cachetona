import sys;sys.path.append(sys.path[0]+'/../')
from Controladores.Main import Main
import tornado.web
from Configuracion.Ecosistema import Ecosistema

eco=Ecosistema()

RUTAS_API=[
    (r"/api/(.*)/(.*)", Main),
    (r'/static/(.*)', tornado.web.StaticFileHandler, {'path': "%s/%s"%(sys.path[0],eco.conf("STATIC_PATH"))}),
    ]
